import chai from 'chai';

// Src
import { convert2Json } from '../src/index';

// TestSamples
import jsonDestinationException from './TestSamples/json/SAMPLE1_DELIMITER.json';
import jsonDestinationStation from './TestSamples/json/SAMPLE2_DELIMITER.json';
import jsonDestinationPrdServices from './TestSamples/json/SAMPLE3_DELIMITER.json';

const expect = chai.expect;

/**
 * @param {TestingParams} TestingParams -
 * @return {void}                       -
 */
function testConvert2json({
  stdConfiguration,
  validate = () => {},
}) {
  let response = null;
  let errMsg = null;
  try {
    response = convert2Json(stdConfiguration);
  } catch (error) {
    errMsg = error.message;
  }

  validate(errMsg, response);
}

describe('Acceptance Tests', () => {
  describe('when converting pipe CSV files', () => {
    context('with sample delimiter file', () => {
      const fileName = 'SAMPLE1_DELIMITER';
      const stdConfiguration = {
        type: 'delimiter',
        fields: [
          { name: 'countryCode', type: 'string' },
          { name: 'city', type: 'string' },
          { name: 'state', type: 'string' },
          { name: 'from_postcode', type: 'string' },
          { name: 'to_postcode', type: 'string' },
          { name: 'productCode', type: 'string' },
          { name: 'featureCode', type: 'string' },
        ],
        separator: '|',
      };
      it('should return valid converted object', async () => {
        stdConfiguration.input = `${__dirname}/TestSamples/raw/${fileName}`;
        testConvert2json({
          stdConfiguration,
          validate: (errMsg, response) => {
            expect(errMsg).to.deep.equal(null);
            expect(response).to.deep.equal(jsonDestinationException);
          },
        });
      });
      it('should return create a valid json file', async () => {
        const configuration = JSON.parse(JSON.stringify(stdConfiguration));
        configuration.input = `${__dirname}/TestSamples/raw/${fileName}`;
        configuration.output = `${__dirname}/result/${fileName}.json`;
        testConvert2json({
          stdConfiguration: configuration,
          validate: (errMsg) => {
            expect(errMsg).to.deep.equal(null);

            // The result is the created file
            const result = require(configuration.output); // eslint-disable-line global-require
            expect(result).to.deep.equal(jsonDestinationException);
          },
        });
      });
    });
    context('with SAMPLE2_DELIMITER', () => {
      const fileName = 'SAMPLE2_DELIMITER';
      const stdConfiguration = {
        type: 'delimiter',
        fields: [
          { name: 'countryCode', type: 'string' },
          { name: 'city', type: 'string' },
          { name: 'state', type: 'string' },
          { name: 'from_postcode', type: 'string' },
          { name: 'to_postcode', type: 'string' },
          { name: 'productcode', type: 'string' },
          { name: 'from_weight', type: 'string' },
          { name: 'to_weight', type: 'string' },
          { name: 'servicectr_reamusid', type: 'string' },
          { name: 'tour_reamusid', type: 'string' },
        ],
        separator: '|',
        input: `${__dirname}/TestSamples/raw/${fileName}`,
      };
      it('should return valid converted object', async () => {
        testConvert2json({
          stdConfiguration,
          validate: (errMsg, response) => {
            expect(errMsg).to.deep.equal(null);
            expect(response).to.deep.equal(jsonDestinationStation);
          },
        });
      });
      it('should return create a valid json file', async () => {
        const configuration = JSON.parse(JSON.stringify(stdConfiguration));
        configuration.output = `${__dirname}/result/${fileName}.json`;
        testConvert2json({
          stdConfiguration: configuration,
          validate: (errMsg) => {
            expect(errMsg).to.deep.equal(null);

            // The result is the created file
            const result = require(configuration.output); // eslint-disable-line global-require
            expect(result).to.deep.equal(jsonDestinationStation);
          },
        });
      });
      it('should return json without first line', async () => {
        const configuration = JSON.parse(JSON.stringify(stdConfiguration));
        configuration.ignoreLines = [1]; // Current first line can't be parsed
        testConvert2json({
          stdConfiguration: configuration,
          validate: (errMsg, response) => {
            expect(errMsg).to.deep.equal(null);

            const expectedResponse = JSON.parse(JSON.stringify(jsonDestinationStation));
            delete expectedResponse.shift();
            expect(response).to.deep.equal(expectedResponse);
          },
        });
      });
    });
    context('with different delimiter file', () => {
      const fileName = 'SAMPLE3_DELIMITER';
      const stdConfiguration = {
        type: 'delimiter',
        fields: [
          { name: 'servicectr_reamusid', type: 'string' },
          { name: 'productCode', type: 'string' },
          { name: 'featureCode', type: 'string' },
          { name: 'allowed', type: 'boolean' },
        ],
        separator: '|',
        input: `${__dirname}/TestSamples/raw/${fileName}`,
        ignoreLines: [1], // Current first line can't be parsed
      };
      it('should return mapped "allowed" values', async () => {
        testConvert2json({
          stdConfiguration,
          validate: (errMsg, response) => {
            expect(errMsg).to.deep.equal(null);
            expect(response).to.deep.equal(jsonDestinationPrdServices);
          },
        });
      });
    });
  });
});
