import chai from 'chai';
import { convert2Json } from '../src/index';

const expect = chai.expect;

describe('Conversion of', () => {
  describe('fixed length file', () => {
    context('without delimiter when called', () => {
      it('returns successful with ACTIVATE.148', async () => {
        try {
          const res = convert2Json(
            {
              type: 'FIXEDLENGTH',
              fields: [{
                name: 'version',
                type: 'number',
                length: 3,
              },
              {
                name: 'date',
                type: 'string',
                length: 10,
              }],
              input: `${__dirname}/TestSamples/raw/ACTIVATE.148`,
            });
          expect(res).to.be.an('array');
          expect(res[0]).to.have.property('version')
          .to.be.equal(148);
          expect(res[0]).to.have.property('date')
          .to.be.equal('12/01/2018');
        } catch (err) {
          throw err;
        }
      });
      it('returns successful with test.abc', async () => {
        try {
          const res = convert2Json(
            {
              type: 'FIXEDLENGTH',
              fields: [{
                name: 'version',
                type: 'number',
                length: 3,
              },
              {
                name: 'date',
                type: 'string',
                length: 10,
              },
              {
                name: 'float',
                type: 'float',
                length: 4,
              },
              {
                name: 'booLean',
                type: 'booLean',
                length: 1,
              },
              {
                name: 'Number',
                type: 'Number',
                length: 3,
              }],
              input: `${__dirname}/TestSamples/raw/test.abc`,
              output: `${__dirname}/result/testOutput.abc`,
            });
          expect(res).to.be.an('array');
          expect(res[0]).to.have.property('version')
          .to.be.equal(148);
          expect(res[0]).to.have.property('date')
          .to.be.equal('12/01/2018');
          expect(res[0]).to.have.property('float')
          .to.be.equal(0.22);
          expect(res[0]).to.have.property('booLean')
          .to.be.equal(true);
          expect(res[0]).to.have.property('Number')
          .to.be.equal(123);
          expect(res[1]).to.have.property('version')
          .to.be.equal(149);
          expect(res[1]).to.have.property('date')
          .to.be.equal('12/02/2018');
          expect(res[1]).to.have.property('float')
          .to.be.equal(1.00);
          expect(res[1]).to.have.property('booLean')
          .to.be.equal(false);
          expect(res[1]).to.have.property('Number')
          .to.be.equal(121);
        } catch (err) {
          throw err;
        }
      });
    });

    context('without definition file when called', () => {
      it('returns unsuccessful error message', async () => {
        try {
          convert2Json();
        } catch (err) {
          expect(err.message).to.equal('Please provide configuration file');
        }
      });
    });
    context('without fields in definition file when called', () => {
      it('returns unsuccessful error message', async () => {
        try {
          convert2Json(
            {
              type: 'FIXEDLENGTH',
              input: `${__dirname}/TestSamples/raw/ACTIVATE.148`,
            }
            );
        } catch (err) {
          expect(err.message).to.equal('Please provide `fields` in configuration file');
        }
      });
    });
    context('without input in definition file when called', () => {
      it('returns unsuccessful error message', async () => {
        try {
          convert2Json(
            {
              type: 'FIXEDLENGTH',
              fields: [{
                name: 'serviceId',
                type: 'number',
                length: 3,
              }],
            }
            );
        } catch (err) {
          expect(err.message).to.equal('Please provide `input` in configuration file');
        }
      });
    });
    context('with wrong type of ignoreLines when called', () => {
      it('returns unsuccessful error message', async () => {
        try {
          convert2Json(
            {
              type: 'FIXEDLENGTH',
              fields: [{
                name: 'serviceId',
                type: 'number',
                length: 3,
              }],
              input: `${__dirname}/TestSamples/raw/ACTIVATE.148`,
              ignoreLines: {},
            }
            );
        } catch (err) {
          expect(err.message).to.equal('ignoreLines type only allows an `array`');
        }
      });
    });
    context('with wrong data in ignoreLines when called', () => {
      it('returns unsuccessful error message', async () => {
        try {
          convert2Json(
            {
              type: 'FIXEDLENGTH',
              fields: [{
                name: 'serviceId',
                type: 'number',
                length: 3,
              }],
              input: `${__dirname}/TestSamples/raw/ACTIVATE.148`,
              ignoreLines: [0],
            }
            );
        } catch (err) {
          expect(err.message).to.equal('`ignoreLines` cannot accept `0`. It only starts from `1`');
        }
      });
    });
    context('without directory writign output when called', () => {
      it('returns unsuccessful error message', async () => {
        try {
          convert2Json(
            {
              type: 'FIXEDLENGTH',
              fields: [{
                name: 'serviceId',
                type: 'number',
                length: 3,
              }],
              input: `${__dirname}/TestSamples/raw/ACTIVATE.148`,
              output: `${__dirname}/test/ACTIVATE.148`,
            }
            );
        } catch (err) {
          expect(err.message).to.equal('Error writing output file');
        }
      });
    });
    context('without delimiter when called', () => {
      it('returns successful with SERVICE.148', async () => {
        try {
          const res = convert2Json(
            {
              type: 'FIXEDLENGTH',
              fields: [{
                name: 'serviceId',
                type: 'number',
                length: 3,
              },
              {
                name: 'serviceDescription',
                type: 'string',
                length: 45,
              },
              {
                name: 'productLine1',
                type: 'string',
                length: 15,
              },
              {
                name: 'productLine2',
                type: 'string',
                length: 35,
              },
              {
                name: 'productCode',
                type: 'string',
                length: 2,
              },
              {
                name: 'dateCode',
                type: 'string',
                length: 2,
              },
              {
                name: 'dayText',
                type: 'string',
                length: 1,
              },
              {
                name: 'timeCode',
                type: 'string',
                length: 1,
              },
              {
                name: 'timeText',
                type: 'string',
                length: 1,
              },
              {
                name: 'serviceCode',
                type: 'string',
                length: 10,
              },
              {
                name: 'featureId',
                type: 'string',
                length: 3,
              },
              {
                name: 'featureCode',
                type: 'string',
                length: 2,
              },
              {
                name: 'fileType',
                type: 'string',
                length: 3,
              },
              {
                name: 'consignmentFlag',
                type: 'string',
                length: 1,
              }],
              input: `${__dirname}/TestSamples/raw/SERVICE.148`,
            });
          expect(res).to.be.an('array');
          expect(res[0]).to.have.property('serviceId')
          .to.be.equal(10);
          expect(res[0]).to.have.property('serviceDescription')
          .to.be.equal('PRIORITY 12:00');
          expect(res[0]).to.have.property('productLine1')
          .to.be.equal('VAN MON TO FRI');
          expect(res[0]).to.have.property('productLine2')
          .to.be.equal('PRE 12 POD');
          expect(res[0]).to.have.property('productCode')
          .to.be.equal('01');
          expect(res[0]).to.have.property('dateCode')
          .to.be.equal('00');
          expect(res[0]).to.have.property('dayText')
          .to.be.equal('');
          expect(res[0]).to.have.property('timeCode')
          .to.be.equal('3');
          expect(res[0]).to.have.property('timeText')
          .to.be.equal('3');
          expect(res[0]).to.have.property('serviceCode')
          .to.be.equal('12');
          expect(res[0]).to.have.property('featureId')
          .to.be.equal('001');
          expect(res[0]).to.have.property('featureCode')
          .to.be.equal('01');
          expect(res[0]).to.have.property('fileType')
          .to.be.equal('UKD');
          expect(res[0]).to.have.property('consignmentFlag')
          .to.be.equal('M');
        } catch (err) {
          throw err;
        }
      });
    });
    context('without mandatory when called', () => {
      it('returns error for `type` field', async () => {
        try {
          convert2Json(
            {
              fields: [{
                name: 'version',
                type: 'number',
                length: 3,
              },
              {
                name: 'date',
                type: 'string',
                length: 10,
              }],
              input: `${__dirname}/TestSamples/raw/ACTIVATE.148`,
            });
        } catch (err) {
          expect(err.message).to.equal('`type` is mandatory field');
        }
      });
      it('returns error for `type: different` field', async () => {
        try {
          convert2Json(
            {
              type: 'different',
              fields: [{
                name: 'version',
                type: 'number',
                length: 3,
              },
              {
                name: 'date',
                type: 'string',
                length: 10,
              }],
              input: `${__dirname}/TestSamples/raw/ACTIVATE.148`,
            });
        } catch (err) {
          expect(err.message).to.equal('Please specify type as `FIXEDLENGTH` or `DELIMITER`');
        }
      });
      it('returns error for separator field', async () => {
        try {
          convert2Json(
            {
              type: 'DELIMITER',
              fields: [{
                name: 'version',
                type: 'number',
                length: 3,
              },
              {
                name: 'date',
                type: 'string',
                length: 10,
              }],
              input: `${__dirname}/TestSamples/raw/ACTIVATE.148`,
            });
        } catch (err) {
          expect(err.message).to.equal(
            '`separator` is mandatory when use used with `type: DELIMITER`'
          );
        }
      });
      it('returns error for name field', async () => {
        try {
          convert2Json(
            {
              type: 'FIXEDLENGTH',
              fields: [{
                type: 'number',
                length: 3,
              },
              {
                name: 'date',
                type: 'string',
                length: 10,
              }],
              input: `${__dirname}/TestSamples/raw/ACTIVATE.148`,
            });
        } catch (err) {
          expect(err.message).to.equal('`name`, `type` and `length` fields are mandatory');
        }
      });
      it('returns error for type field in fields', async () => {
        try {
          convert2Json(
            {
              type: 'FIXEDLENGTH',
              fields: [{
                name: 'version',
                length: 3,
              },
              {
                name: 'date',
                type: 'string',
                length: 10,
              }],
              input: `${__dirname}/TestSamples/raw/ACTIVATE.148`,
            });
        } catch (err) {
          expect(err.message).to.equal('`name`, `type` and `length` fields are mandatory');
        }
      });
      it('returns error for length field in fields', async () => {
        try {
          convert2Json(
            {
              type: 'FIXEDLENGTH',
              fields: [{
                name: 'version',
                type: 'number',
              },
              {
                name: 'date',
                type: 'string',
                length: 10,
              }],
              input: `${__dirname}/TestSamples/raw/ACTIVATE.148`,
            });
        } catch (err) {
          expect(err.message).to.equal('`name`, `type` and `length` fields are mandatory');
        }
      });
      it('returns error for wrong field type', async () => {
        try {
          convert2Json(
            {
              type: 'FIXEDLENGTH',
              fields: [{
                name: 'version',
                type: 'abc',
                length: 3,
              },
              {
                name: 'date',
                type: 'string',
                length: 10,
              }],
              input: `${__dirname}/TestSamples/raw/ACTIVATE.148`,
            });
        } catch (err) {
          expect(err.message).to.equal(
            '`type` must be set to either `string`, `boolean`, `number` or `float`'
          );
        }
      });
      it('returns error for typeof fields as object', async () => {
        try {
          convert2Json(
            {
              type: 'FIXEDLENGTH',
              fields: {
                name: 'version',
                type: 'number',
                length: 3,
              },
              input: `${__dirname}/TestSamples/raw/ACTIVATE.148`,
            });
        } catch (err) {
          expect(err.message).to.equal('`fields` type must be Array');
        }
      });
    });
  });
  describe('non-fixed length file', () => {
    context('with delimiter when called', () => {
      it('returns successful result with REAMUSID.148', async () => {
        try {
          const res = convert2Json(
            {
              type: 'DELIMITER',
              fields: [{
                name: 'reamusId',
                type: 'number',
                length: 6,
              },
              {
                name: 'locationName',
                type: 'string',
                length: 35,
              },
              {
                name: 'opUnit',
                type: 'number',
                length: 6,
              },
              {
                name: 'countryCode',
                type: 'string',
                length: 2,
              }],
              input: `${__dirname}/TestSamples/raw/REAMUSID.148`,
              separator: '|',
            });
          expect(res).to.be.an('array');
          expect(res[0]).to.have.property('reamusId')
          .to.be.equal(148);
        } catch (err) {
          throw err;
        }
      });
      it('returns error for `name` type field', async () => {
        try {
          convert2Json(
            {
              type: 'DELIMITER',
              fields: [{
                type: 'number',
                length: 6,
              },
              {
                name: 'locationName',
                type: 'string',
                length: 35,
              },
              {
                name: 'opUnit',
                type: 'number',
                length: 6,
              },
              {
                name: 'countryCode',
                type: 'string',
                length: 2,
              }],
              input: `${__dirname}/TestSamples/raw/REAMUSID.148`,
              separator: '|',
            });
        } catch (err) {
          expect(err.message).to.equal('`name` and `type` fields are mandatory');
        }
      });
      it('returns error for wrong field type', async () => {
        try {
          convert2Json(
            {
              type: 'DELIMITER',
              fields: [{
                name: 'reamusId',
                type: 'abc',
                length: 6,
              },
              {
                name: 'locationName',
                type: 'string',
                length: 35,
              },
              {
                name: 'opUnit',
                type: 'number',
                length: 6,
              },
              {
                name: 'countryCode',
                type: 'string',
                length: 2,
              }],
              input: `${__dirname}/TestSamples/raw/REAMUSID.148`,
              separator: '|',
            });
        } catch (err) {
          expect(err.message).to.equal(
            '`type` must be set to either `string`, `boolean`, `number` or `float`'
          );
        }
      });
    });
  });
});
