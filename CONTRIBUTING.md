## Gitlab Installation

<!-- TOC -->

- [Gitlab Installation](#gitlab-installation)
  - [Deployment](#deployment)
  - [Testing](#testing)
    - [CLI](#cli)

<!-- /TOC -->

### Deployment

Set the required variables for deployment via Gitlab.   

Below includes examples:

| Variable             | Description       | Example |
| ---------            | ----------------- | --------|
| `NPM_TOKEN`          | NPM Publish Token | `abc123`|

### Testing

To run tests, execute the following command:

```
yarn test
```
This will run all unit tests and acceptance-tests covered in library

```
yarn coverage
```
This will execute all tests and provide you with coverage report with minimum threshold set to 90%

#### CLI

Command line can be runned locally to test conversion using tests fixtures
```sh
  yarn start convert -- -c "$PWD/test/fixtures/raw/CLI.json"
```

/!\ You will have update `input` and `output` paths in `test/fixtures/raw/CLI.json`
