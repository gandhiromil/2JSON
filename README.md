# json-convert

> Latest stable version: `1.0.1`
<!-- TOC -->

- [json-convert](#json-convert)
- [Generator](#generator)
  - [Capabilities](#capabilities)
  - [How to use](#how-to-use)
    - [Install](#install)
    - [As a module](#as-a-module)
    - [CLI](#cli)
      - [Usage:](#usage)
    - [Configuration File](#configuration-file)
      - [Example of FIXEDLENGTH configuration file](#example-of-fixedlength-configuration-file)
      - [Example of DELIMITER configuration file](#example-of-delimiter-configuration-file)

<!-- /TOC -->

# Generator
[![build status](https://gitlab.com/gandhiromil/2JSON/badges/master/build.svg)] (https://gitlab.com/gandhiromil/2JSON/commits/master)

## Capabilities

- Convert fixed-length and CSV type files to JSON format
- features: ignoreLines, data type conversion
- CLI interface with `convert` command
- Prettified JSON output

## How to use

### Install

convert2json library should be used as a *devDependencies*

```javascript
npm install convert2json --save-dev
```

or

```javascript
yarn add convert2json -D
```

### As a module

```js
// Import
import { convert2Json } from 'convert2json';

// Convert
const configuration = {
  input: `${filePath}`,
  type: 'delimiter',
  separator: '|',
  fields: [{
    name: 'fields',
    type: 'string',
  }],
};

const converted = convert2Json(configuration);

```

### CLI

json-convert offers a CLI command: `convert`.

```javascript
Usage: yarn json-convert convert -- [options]

Commands:
  convert [options]      Converts files to JSON output

Options:
  -c, --configuration  set configuration path of your configuration file

```
#### Usage:
```javascript
  yarn json-convert convert -- -c '/path/to/your/configuration/file.json'
```

### Configuration File

- The Configuration is `JSON` file which accepts different parameters.
- The parameters includes:
  - `type` - It is type of file you wanted to convert. It accepts `FIXEDLENGTH` and `DELIMITER`
  - `input` - It accepts path for your source file
  - `output` - It accepts path to save your output JSON file
  - `ignoreLines` - It will skips the lines mentioned in an `array`
  - `separator` - It will accept separator parameter (`|` or `,`)
  - `fields` - This is an `array` of `objects` accepting various parameters
    #### fields parameters
       - "name" - Expected name of the field in JSON output,
       - "type" - Data type of the field,
       - "length" - specifies length of the field specified

#### Example of FIXEDLENGTH configuration file
```javascript
{
  "type": "FIXEDLENGTH",
  "input": "/home/Projects/json-convert-library/fixtures/ACTIVATE.148",
  "output": "/home/Projects/json-convert-library/abc.JSON",
  "ignoreLines": [1, 23, 99],
  "fields": [{
    "name": "version",
    "type": "float",
    "length": 3
  },
  {
    "name": "date",
    "type": "string",
    "length": 10
  }]
}
```

#### Example of DELIMITER configuration file
```javascript
{
  "type": 'DELIMITER',
  "input": "/home/Projects/json-convert-library/fixtures/REAMUSID.148",
  "output": "/home/Projects/json-convert-library/abc.JSON",
  "separator": "|",
  "ignoreLines": [1, 23, 99],
  "fields": [{
    "name": "reamusId",
    "type": "abc",
    "length": 6
  },
  {
    "name": "locationName",
    "type": "string",
    "length": 35
  },
  {
    "name": "opUnit",
    "type": "number",
    "length": 6
  },
  {
    "name": "countryCode",
    "type": "string",
    "length": 2
  }]
}
```
