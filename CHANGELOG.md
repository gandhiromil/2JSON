# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.1][] - 2018-05-04

### Added
- update coverage in README.md

## [1.0.0][] - 2018-05-04

### Added
- CHANGELOG.md
- CONTRIBUTING.md
- update README.md for build status and coverage status
- Added ignoreLines feature for bypass the non-required lines
- Added function to convert fixed-length file to JSON output
- Added function to convert specific length file to JSON output
- Added feature to convert datatype to string, number and boolean
- Added CLI command 'convert' to use it from terminal
- Added Acceptance tests
- Error handling is implemented


[Unreleased]: https://gitlab.com/gandhiromil/2JSON/compare/v1.0.0...master
[1.0.0]: https://gitlab.com/gandhiromil/2JSON/tree/master


[Unreleased]: undefined/compare/v1.0.1...HEAD
[1.0.1]: undefined/tree/v1.0.1