import { lineErrors, fieldByType } from './common';

/**
 * @param {object} fields - actual data file with max limit of an value
 * @param {string} line - each line from file
 * @param {string} separator - mandatory value when used with DELIMITER to seperate columns
 *
 * @return {object} mapping key and value using separator
 */
function lineDelimiter2Object(fields, line, separator) {
  const results = {};

  fields.forEach((field, index) => {
    lineErrors(field, 'delimiter');
    // name is field name, type is type of data and length is length need to cut
    const { name, type } = field;
    const data = line.split(separator);

    results[name] = fieldByType(type, data[index]);
  });

  return results;
}

/**
 * @param {array} fields - fields from configuration file
 * @param {string} lines - Each Line form raw file
 * @param {array} ignoreLines - to ignore some spoecific lines from raw file
 * @param {string} separator - to define DELIMITER or FIXEDLENGTH file
 *
 * @return {array} results
 */
function toJson(fields, lines, ignoreLines, separator) {
  const results = [];
  lines.forEach((item, index) => {
    if (ignoreLines && ignoreLines.includes(index + 1)) {
      return;
    }
    const columnMap = lineDelimiter2Object(fields, item, separator);
    results.push(columnMap);
  });

  return results;
}

export { toJson, lineDelimiter2Object };
