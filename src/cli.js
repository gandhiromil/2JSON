#!/usr/bin/env node
import program from 'commander';

import { configErrors } from './common';
import { convert2Json } from './index';

program
  .command('convert')
  .description('convert from fix length or csv\'s to json')
  .option('-c, --configuration <configuration>', 'set configuration path of your source file')
  .action((options) => {
    const { configuration } = options;
    const definitionFile = configErrors(configuration);
    convert2Json(definitionFile);
  });

program.parse(process.argv);
