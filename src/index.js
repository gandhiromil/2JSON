import fs from 'fs';

import * as fileWithDelimiter from './fileWithDelimiter';
import * as fileWithFixedLength from './fileWithFixedLength';

/**
 * @param {string} definitionFile - includes data, compress and separator
 *
 * @return {array} array of JSON objects
 */
function convert2Json(definitionFile) {
  if (!definitionFile) {
    throw new Error('Please provide configuration file');
  }
  const { input, fields, separator, type, output, ignoreLines } = definitionFile;
  if (!fields) {
    throw new Error('Please provide `fields` in configuration file');
  }
  if (!Array.isArray(fields)) {
    throw new Error('`fields` type must be Array');
  }
  if (!type) {
    throw new Error('`type` is mandatory field');
  }
  if (!input) {
    throw new Error('Please provide `input` in configuration file');
  }
  const uppedType = type.toUpperCase();
  if (!['FIXEDLENGTH', 'DELIMITER'].includes(uppedType)) {
    throw new Error('Please specify type as `FIXEDLENGTH` or `DELIMITER`');
  }
  if (uppedType === 'DELIMITER' && !separator) {
    throw new Error('`separator` is mandatory when use used with `type: DELIMITER`');
  }
  if (ignoreLines) {
    if (!Array.isArray(ignoreLines)) throw new Error('ignoreLines type only allows an `array`');
    if (ignoreLines.length > 0) {
      ignoreLines.forEach((number) => {
        if (!(+number && +number > 0 && Math.floor(number) === +number)) {
          throw new Error('`ignoreLines` cannot accept `0`. It only starts from `1`');
        }
      });
    }
  }

  const rawFile = fs.readFileSync(input).toString().trim();
  const lines = rawFile.split('\r').toString().split('\n');
  let result;
  if (uppedType === 'DELIMITER') {
    result = fileWithDelimiter.toJson(fields, lines, ignoreLines, separator);
  } else if (uppedType === 'FIXEDLENGTH') {
    result = fileWithFixedLength.toJson(fields, lines, ignoreLines);
  } else {
    throw Error;
  }

  if (output) {
    try {
      fs.writeFileSync(`${output}`, JSON.stringify(result, null, 2));
    } catch (e) {
      throw new Error('Error writing output file', e);
    }
  }

  return result;
}

export { convert2Json };
