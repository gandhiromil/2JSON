import fs from 'fs';

/**
 * @param {object} configFile - configuration file
 * @param {string} call - if call from cli then call convert2Json
 *
 * @return {string} error if any
 */
function configErrors(configFile) {
  try {
    return JSON.parse(fs.readFileSync(configFile, 'utf8'));
  } catch (e) {
    throw new Error('Something\'s wrong in configuration file', e);
  }
}

/**
 * @param {object} field - Individual line to validate errors
 * @param {string} functionType - fixLength or delimiter
 *
 * @return {string} error if any
 */
function lineErrors(field, functionType) {
  const allowedTypes = ['STRING', 'BOOLEAN', 'NUMBER', 'FLOAT'];
  const functype = functionType.toUpperCase();
  const { name, type, length } = field;

  if (type && !allowedTypes.includes(type.toUpperCase())) {
    throw new Error('`type` must be set to either `string`, `boolean`, `number` or `float`');
  }
  if (functype === 'FIXEDLENGTH' && (!name || !type || !length)) {
    throw new Error('`name`, `type` and `length` fields are mandatory');
  }
  if (functype === 'DELIMITER' && (!name || !type)) {
    throw new Error('`name` and `type` fields are mandatory');
  }
}

/**
 * @param {object} type - dataType required for specific output field
 * @param {string} value - fixLength or delimiter
 *
 * @return {string|boolean|number|float} result
 */
function fieldByType(type, value) {
  const dataType = type.toUpperCase();
  if (dataType === 'NUMBER') {
    const result = +value;
    if (isNaN(result)) throw new Error(`Cannot convert ${value} to number`);
    return result;
  }

  if (dataType === 'BOOLEAN') {
    return (value.toUpperCase() === 'TRUE' || value.toUpperCase() === 'Y');
  }

  if (dataType === 'FLOAT') {
    if (value === '') {
      return 0;
    }
    const result = parseFloat(value);
    if (isNaN(result)) throw new Error(`Cannot convert ${value} to float`);
    return result;
  }

  return value;
}

export { configErrors, lineErrors, fieldByType };
