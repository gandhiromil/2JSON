
import { lineErrors, fieldByType } from './common';

/**
 * @param {object} fields - actual data file with max limit of an value
 * @param {string}  line - each line from file
 *
 * @return {object} mapping key and value using max limit provided
 */
function lineFixLength2Object(fields, line) {
  if (!line) {
    throw new Error('items is not exist!');
  }
  let fromCut = 0;
  const results = {};
  fields.forEach((field) => {
    lineErrors(field, 'fixedLength');
    // name is field name, type is type of data and length is length need to cut
    const { name, type, length } = field;
    const value = line.substring(fromCut, fromCut + +length).trim();

    results[name] = fieldByType(type, value);

    fromCut += length;
  });

  return results;
}

/**
 * @param {array} fields - fields from configuration file
 * @param {string} lines - Each Line form raw file
 * @param {array} ignoreLines - to ignore some spoecific lines from raw file
 * @param {string} separator - to define DELIMITER or FIXEDLENGTH file
 *
 * @return {array} results
 */
function toJson(fields, lines, ignoreLines) {
  const results = [];
  lines.forEach((item, index) => {
    if (ignoreLines && ignoreLines.includes(index + 1)) {
      return;
    }
    const columnMap = lineFixLength2Object(fields, item);
    results.push(columnMap);
  });

  return results;
}

export { lineFixLength2Object, toJson };
